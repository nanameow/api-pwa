import { mount } from '@vue/test-utils'
import Search from '@/components/Search.vue'

describe('Search.vue', () => {
  const wrapper = mount(Search)
  it('renders the correct markup', () => {
    expect(wrapper.html()).toContain('<div class="row">')
  })

  const div = wrapper.find('div')
  it('there is a div', () => {
    expect(div.exists()).toBe(true)
  })

  it('does not render a button', () => {
    expect(wrapper.find('button').exists()).toBe(false)
  })
})
